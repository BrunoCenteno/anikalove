from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort

from flaskr.auth import login_required
from flaskr.db import get_db

bp = Blueprint('blog', __name__)


@bp.route('/')
def index():
    return render_template('Menu.html')

@bp.route('/<name>')
def hello(name=None):
    username = name
    #Check if the user exists
    if get_db().execute(
    'SELECT id FROM user WHERE username = ?', (username,)
    ).fetchone() is not None:
        sections = get_db().execute(
        'SELECT p.id, title FROM section p JOIN user u ON p.author_id = u.id WHERE username = ?', (username,)
        ).fetchall()
        items = get_db().execute(
        'SELECT p.id, title, body, author_id, username, section_id, price FROM item p JOIN user u ON p.author_id = u.id WHERE username = ? ORDER BY created DESC', (username,)
        ).fetchall()
        return render_template('blog/index.html', items=items, name=name, sections=sections)
    else:
        return render_template('hello.html', name=name + ' no existe')

@bp.route('/<name>/qr')
def QR(name=None):
    return render_template('QR.html', name=name)

@bp.route('/scan')
def scan():
    return render_template('scan.html')


@bp.route('/<int:section_id>/create_item', methods=('GET', 'POST'))
@login_required
def create_item(section_id):
    section = section_id
    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        price = request.form['price']
        error = None
        if not title:
            error = 'Por favor agrega un titulo.'

        if section_id is None:
            error = 'no section_id'
        if price is None:
            error = 'no price'

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'INSERT INTO item (title, body, author_id, section_id, price)' 
                ' VALUES (?, ?, ?, ?, ?)',
                (title, body, g.user['id'], section, price)
            )
            db.commit()
            return hello(name=g.user['username'])

    return render_template('blog/create_item.html')

@bp.route('/create_section', methods=('GET', 'POST'))
@login_required
def create_section():
    if request.method == 'POST':
        title = request.form['title']
        error = None
        
        if not title:
            error = 'Por favor agrega un titulo.'

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'INSERT INTO section (title, author_id)' 
                ' VALUES (?, ?)',
                (title, g.user['id'])
            )
            db.commit()
            return hello(name=g.user['username'])

    return render_template('blog/create_section.html')

def get_item(id, check_author=True):
    item = get_db().execute(
        'SELECT p.id, title, body, author_id, username, section_id, price'
        ' FROM item p JOIN user u ON p.author_id = u.id'
        ' WHERE p.id = ?',
        (id,)
    ).fetchone()

    if item is None:
        abort(404, "item id {0} doesn't exist.".format(id))

    if check_author and item['author_id'] != g.user['id']:
        abort(403)
    return item

def get_section(id, check_author=True):
    section = get_db().execute(
        'SELECT p.id, title, author_id, username'
        ' FROM section p JOIN user u ON p.author_id = u.id'
        ' WHERE p.id = ?',
        (id,)
    ).fetchone()

    if section is None:
        abort(404, "section id {0} doesn't exist.".format(id))

    if check_author and section['author_id'] != g.user['id']:
        abort(403)
    return section

#Update a section
@bp.route('/<int:id>/update_section', methods=('GET', 'POST'))
@login_required
def update_section(id):
    section = get_section(id)

    if request.method == 'POST':
        title = request.form['title']
        error = None

        if not title:
            error = 'Title is required.'

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'UPDATE section SET title = ?'
                ' WHERE id = ?',
                (title, id)
            )
            db.commit()
            return hello(name=g.user['username'])

    return render_template('blog/update_section.html', section=section)

#Update an item
@bp.route('/<int:id>/update_item', methods=('GET', 'POST'))
@login_required
def update_item(id):
    item = get_item(id)

    if request.method == 'POST':
        title = request.form['title']
        error = None

        if not title:
            error = 'Title is required.'

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'UPDATE item SET title = ?'
                ' WHERE id = ?',
                (title, id)
            )
            db.commit()
            return hello(name=g.user['username'])

    return render_template('blog/update_item.html', post=item)

#Delete a section
@bp.route('/<int:id>/delete_section', methods=('POST',))
@login_required
def delete_section(id):
    get_section(id)
    db = get_db()
    db.execute('DELETE FROM section WHERE id = ?', (id,))
    db.commit()
    return hello(name=g.user['username'])
#Delete an item
@bp.route('/<int:id>/delete_item', methods=('POST',))
@login_required
def delete_item(id):
    get_item(id)
    db = get_db()
    db.execute('DELETE FROM item WHERE id = ?', (id,))
    db.commit()
    return hello(name=g.user['username'])